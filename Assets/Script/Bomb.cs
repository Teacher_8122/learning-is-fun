﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    bool _hasColided = false;
    [SerializeField]
    float _tick = 0.5f;
    Vector3 _velocity;
    private Animator _myAnim;
    private ParticleSystem _myPartSys;
	//Use this for initialization
    void Start()
    {
        _myAnim = GetComponent<Animator>();
        _myPartSys = GetComponent<ParticleSystem>();

        if (transform.parent)
            transform.SetPositionAndRotation(transform.parent.position, transform.parent.rotation);

        _velocity = _velocity.normalized;
        _velocity = transform.up * 16 * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, transform.position.y,-0.6f);

        transform.parent = null;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _hasColided = true;
        _velocity *= 0.9f;
    }

    //Update is called once per frame
    void Update()
    {
        transform.position += _velocity;
        if (_hasColided)
            Detonation();
    }

    private void Detonation()
    {
        _tick -= Time.deltaTime;
        transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime,0);

        if(_tick < 0)
        {
            Destroy(gameObject);
        }
        else if (_tick < 0.35f)
        {
            _myAnim.enabled = true;
            _myPartSys.Play();
        }
    }
}
