﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ufo : MonoBehaviour
{
    static GameObject s_target = null;
    private float _speedMod;
    Vector3 _velocity;

    private float _tick = 0.49f;
    bool _hasColided = false;
	// Use this for initialization
	void Start()
    {
        if (!s_target)
            print("Target missing from UFO script");

        _speedMod = Random.Range(0.05f, 0.1f);
        _velocity = s_target.transform.position - transform.position;
        _velocity = _velocity.normalized;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _velocity = s_target.transform.position - transform.position;
        _velocity = _velocity.normalized;

        if (collision.gameObject.GetComponent<Bullet>())
            Destroy(gameObject);
        if (collision.gameObject.GetComponent<Bomb>())
        {
            _hasColided = true;
            _speedMod *= 1.1f;
            _velocity = new Vector3(Mathf.Sin(Time.deltaTime),Mathf.Cos(Time.deltaTime),0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        _speedMod += (Time.deltaTime * 0.01f);
        transform.position += _velocity * _speedMod;

        if(_hasColided)
        {
            _tick -= Time.deltaTime;
            if(_tick <= 0)
                Destroy(gameObject);
        }
	}

    public static GameObject Target
    {
       set { s_target = value; }
    }
}
