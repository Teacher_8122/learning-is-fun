﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Vector3 _velocity;
    // Use this for initialization
    [SerializeField]
    float time;
    float _tick;

    void Start()
    {
        _velocity = _velocity.normalized;
        _velocity = transform.up * 20 * Time.deltaTime;
    }

    private void OnEnable()
    {
        _tick = time;

        if (transform.parent)
            transform.SetPositionAndRotation(transform.parent.position, transform.parent.rotation);


        _velocity = transform.up * 20 * Time.deltaTime;
        transform.parent = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        _tick -= Time.deltaTime;
        transform.position += _velocity;

        if(_tick <= 0)
            gameObject.SetActive(false);
        
    }
}
