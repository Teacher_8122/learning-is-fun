﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOSpawnner : MonoBehaviour
{
    [SerializeField]
    GameObject _target;
    [SerializeField]
    GameObject _ufo;
    [SerializeField]
    GameObject[] _spawnPoints;
    [SerializeField] 
    float _maxRate = 0, _minRate = 0;
    private float _rate;
    // Use this for initialization
	void Start()
    {
        Ufo.Target = _target;
        _rate = _maxRate;
	}
	
	// Update is called once per frame
	void Update()
    {
        _rate -= Time.deltaTime;

        if(_rate <= 0)
        {
            _rate = (_maxRate -= Time.deltaTime * 0.5f);

            for (int i = 0; i < 3; i++)
            {
                int rand = Random.Range(0, _spawnPoints.Length);
                Instantiate(_ufo, _spawnPoints[rand].transform);
            }


            if (_maxRate < _minRate)
                _rate = _maxRate = _minRate;
        }

	}
}
