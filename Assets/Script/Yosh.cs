﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yosh : MonoBehaviour
{
    [SerializeField]
    GameObject _bullet;
    [SerializeField]
    GameObject _bomb;
    GameObject[] _bulletList;
    uint _iter = 0;
    const uint _poolLimit = 20;
	//Use this for initialization
	void Start ()
    {
        _bulletList = new GameObject[_poolLimit];
        for (int i = 0; i < _poolLimit; i++)
        {
            _bulletList[i] = Instantiate(_bullet, transform.position + (transform.up * 2), Quaternion.identity);
            _bulletList[i].transform.parent = transform;
            _bulletList[i].SetActive(false);
        }
    }
	
	//Update is called once per frame
	void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        Vector3 pos = transform.position;
        pos.z = 0;
        transform.rotation = Quaternion.LookRotation(new Vector3(0,0,1), mousePos - pos);

        if(Input.GetMouseButtonDown(0))
            Fire();
        if (Input.GetMouseButtonDown(1))
            FireBomb();
    }

    void Fire()
    {
        _bulletList[_iter].transform.parent = transform;
        _bulletList[_iter].SetActive(true);
        _iter++;

        if(_iter >= _poolLimit)
            _iter = 0;
    }

    void FireBomb()
    {
        GameObject bomb = Instantiate(_bomb, transform.position + (transform.up * 2), Quaternion.identity);
        bomb.transform.parent = transform;
    }
}
